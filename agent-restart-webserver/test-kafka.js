const fs     = require("fs");
const kafkaConnectionString = "37.48.247.124:2181";
const zkOptions = undefined;// {};
const noAckBatchOptions =  undefined;// {};
const sslOptions = {};

try{
  sslOptions.ca = fs.readFileSync( "./kafka-ca.cert" );
}catch( err ){
  console.error( "Error while reading ssl.ca.location", err.message );
}
//donot verify hostname
sslOptions.rejectUnauthorized = false;

var kafka = require('kafka-node'),
    Consumer = kafka.Consumer,
    client = new kafka.Client(
        kafkaConnectionString, 
        "clientName",
        zkOptions,
        noAckBatchOptions,
        sslOptions
        ),
    consumer = new Consumer(
        client,
        [
        { topic: 'session.flow.report', partition: 0 }
        ],
        {
          autoCommit: false
        }
        );

    consumer.on('message', function (message) {
      console.log(message);
    });
