"use strict";


const config = require('./config.json');
const kafka  = require("./kafka.js");
const exec   = require('child_process').exec;


if( config.topic_name == undefined )
   return console.error("Need topic_name");

if( config.command == undefined )
   return console.error("Need command");

if( ! config.is_in_debug_mode ){
   console.info = function(){};
}else{
   console.info = console.log;
}

const kafkaClient = kafka.createClient()

kafkaClient.subscribe( config.topic_name );
kafkaClient.on("message", function( topic, message ){
   console.info( (new Date()).toLocaleTimeString() + ": " + message );

   exec( config.command , (err, stdout, stderr) => {
      if (err) {
         console.error("Cannot executed ["+ config.command +"]")
         return;
      }

      // the *entire* stdout and stderr (buffered)
      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);
   });
});

console.log( "Configuration:" + JSON.stringify( config ));
console.log('MMT-MUSA-Agent is waiting for commands on kafkabus ' + config.topic_name );

console.log("================================")