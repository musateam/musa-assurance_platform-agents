This folder contains NodeJS code of MMT-MUSA-Agent-Restart-Apache.
It is an agent that receives message from a kafkabus then executes some command.

# Requirement

   Nodejs > v4.0

# Run

   node app.js
   
# Package

   node build.js

# Configuration

- `command`: the bash command to be executed when getting a new message, such as, restart Apache server
- `kafka_server`: parameters allowing to connect to the kafka server
- `topic_name`: name of topic on which the app will listen to receive data
- `is_in_debug_mode`: `true` to print out data received
